# traefik-ip-blocklist-server

Read the post here: [How to block IPs in your Traefik Proxy Server](https://bit.ly/3hQHW8v)

This repo contains files related to the post.

## Up and running
- Clone the repo
- Run `cd traefik-ip-blocklist-server`
- Run `docker-compose up`


>To Traefik Developers: Please make a built in feature.  Traefik is awesome... make it awesomer.

